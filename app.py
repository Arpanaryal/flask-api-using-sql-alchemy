from distutils.version import StrictVersion
from enum import unique
from flask import Flask , request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

#INIT app
app = Flask(__name__)

basedir =os.path.abspath(os.path.dirname(__file__))

#db
app.config['SQLALCHEMY_DATABASE_URI'] ='postgresql://postgres:73014@localhost/flaskcardb' 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False

#init db
db = SQLAlchemy(app)
#inti ma
ma = Marshmallow(app)

#car class/model
class Car(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    brand = db.Column(db.String(50), unique =True)
    cc =db.Column(db.Float)
    maxspeed = db.Column(db.String(100))

    def __init__(self,brand, cc, maxspeed):
        self.brand = brand
        self.cc = cc
        self.maxspeed = maxspeed
#product schema
class CarSchema(ma.Schema):
    class Meta:
        fields = ('id', 'brand', 'cc', 'maxspeed')

#init schema
car_schema = CarSchema()
cars_schema = CarSchema(many = True )


#Create or Post a car
@app.route('/car', methods=['POST'])
def add_car():
    brand = request.json['brand']
    cc = request.json['cc']
    maxspeed = request.json['maxspeed']

    new_car = Car(brand, cc, maxspeed )
    
    db.session.add(new_car)
    db.session.commit()  

    return car_schema.jsonify(new_car)


#get all cars
@app.route('/car', methods =['GET'])
def get_cars():
    all_cars = Car.query.all()
    result = cars_schema.dump(all_cars)
    return jsonify(result)



#get single car
@app.route('/car/<id>', methods =['GET'])
def get_car(id):
    car = Car.query.get(id)
    return car_schema.jsonify(car)


#update car info
@app.route('/car/<id>', methods=['PUT'])
def update_car(id):
    car = Car.query.get(id)

    brand = request.json['brand']
    cc = request.json['cc']
    maxspeed = request.json['maxspeed']

    car.brand = brand
    car.cc = cc
    car.maxspeed = maxspeed
    
    db.session.commit()  

    return car_schema.jsonify(car)

#delete a car
@app.route('/car/<id>', methods =['DELETE'])
def delete_car(id):
    car = Car.query.get(id)
    db.session.delete(car)
    db.session.commit()
    return car_schema.jsonify(car)



#runserver
if __name__ == '__main__':
    app.run(debug=True)